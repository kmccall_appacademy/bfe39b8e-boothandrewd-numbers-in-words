# lib/01_in_words.rb

class Fixnum
  def in_words
    return 'zero' if zero?

    # Initialize base number words
    number_words = {
      11 => 'eleven',
      12 => 'twelve',
      13 => 'thirteen',
      15 => 'fifteen',
      18 => 'eighteen'
    }
    %w[zero one two three four
       five six seven eight nine].each_with_index do |word, index|
      number_words[index] = word
    end
    %w[ten twenty thirty forty fifty
       sixty seventy eighty ninety].each_with_index do |word, index|
      number_words[(index + 1) * 10] = word
    end

    # Initialize 'illion' prefixes
    illion_table = [nil] + %w[thousand million billion trillion]

    parse_two_digits = lambda do |digits|
      return '' if digits.zero?
      return number_words[digits] if number_words.include?(digits)
      if digits / 10 == 1
        number_words[digits % 10] + 'teen'
      else
        number_words[digits / 10 * 10] + ' ' + number_words[digits % 10]
      end
    end

    parse_triplet = lambda do |digits|
      words = if digits >= 100
        number_words[digits / 100] + ' hundred'
      else
        ''
      end + ' ' + parse_two_digits.call(digits % 100)

      words.strip
    end

    to_s.reverse.scan(/.{1,3}/)
        .map(&:reverse).map(&:to_i).map.with_index do |trip, idx|
      words = parse_triplet.call(trip)
      words += (trip.zero? || idx.zero? ? '' : ' ' + illion_table[idx])
    end.reverse.map(&:strip).reject(&:empty?).join(' ').strip
  end
end
